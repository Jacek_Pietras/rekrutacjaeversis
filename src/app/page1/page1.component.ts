import { Router } from '@angular/router';

import { Component, OnInit } from '@angular/core';

import { User } from './../user';

@Component({
  selector: 'app-page1',
  templateUrl: './page1.component.html',
  styleUrls: ['./page1.component.css']
})
export class Page1Component implements OnInit {

  userDataBox = 'User Data';

  model = new User();

  newUser() {
    sessionStorage.setItem('userInfo', JSON.stringify(this.model));
    this.router.navigate(['/page2']);
  }

  constructor( private router: Router ) { }

  ngOnInit() {}

}
