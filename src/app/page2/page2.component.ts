import { Component, OnInit } from '@angular/core';

import { User } from './../user';

@Component({
  selector: 'app-page2',
  templateUrl: './page2.component.html',
  styleUrls: ['./page2.component.css']
})
export class Page2Component implements OnInit {

  createdUser = new User();

  displayImage = false;
  displayInfo = false;

  userAccess() {
    if (this.createdUser.age >= 18) {
      this.displayImage = true;
    } else {
      this.displayInfo = true;
    }
  }

  constructor() {}

  ngOnInit() {
    if (sessionStorage.userInfo) {
      this.createdUser = JSON.parse(sessionStorage.getItem('userInfo'));
    }
  }

}
